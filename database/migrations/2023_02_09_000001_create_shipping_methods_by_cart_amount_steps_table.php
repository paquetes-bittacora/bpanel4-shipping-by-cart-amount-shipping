<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {

    private const TABLE_NAME = 'shipping_methods_by_cart_amount_steps';

    public function up(): void
    {
        Schema::create(self::TABLE_NAME, static function (Blueprint $table): void {
            $table->id();
            $table->unsignedBigInteger('by_cart_amount_id');
            $table->integer('minimum');
            $table->integer('price');
            $table->timestamps();

            $table->foreign('by_cart_amount_id')
                ->references('id')->on('shipping_methods_by_cart_amount');
        });
    }

    public function down(): void
    {
        Schema::drop(self::TABLE_NAME);
    }
};
