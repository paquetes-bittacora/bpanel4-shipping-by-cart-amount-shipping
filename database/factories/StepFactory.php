<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Database\Factories;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\PriceStep;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<PriceStep>
 */
final class StepFactory extends Factory
{
    /**
     * @var class-string<PriceStep>
     */
    protected $model = PriceStep::class;

    /**
     * @return array<string, int>
     * @throws InvalidPriceException
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            'minimum' => new Price(random_int(0, 30)),
            'price' => new Price(random_int(0,50)),
        ];
    }
}
