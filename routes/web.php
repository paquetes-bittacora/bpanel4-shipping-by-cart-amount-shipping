<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Http\Controllers\ByCartAmountAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/envio')->name('bpanel4-shipping.by-cart-amount.bpanel.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/metodos-de-envio/ByCartAmount/{zone}/crear', [ByCartAmountAdminController::class, 'create'])->name('create');
        Route::post('/metodos-de-envio/ByCartAmount/{zone}/crear', [ByCartAmountAdminController::class, 'store'])->name('store');
        Route::get('/metodos-de-envio/ByCartAmount/{model}/editar', [ByCartAmountAdminController::class, 'edit'])->name('edit');
        Route::post('/metodos-de-envio/ByCartAmount/{model}/editar', [ByCartAmountAdminController::class, 'update'])->name('update');
        Route::delete('/metodos-de-envio/ByCartAmount/{model}/eliminar', [ByCartAmountAdminController::class, 'destroy'])->name('destroy');
    });
