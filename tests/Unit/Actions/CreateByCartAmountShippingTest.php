<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Tests\Unit\Actions;

use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Actions\CreateByCartAmountShipping;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Database\Factories\StepFactory;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Dtos\ByCartAmountDto;
use Bittacora\Bpanel4\Shipping\Database\Factories\ShippingZoneFactory;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Throwable;

final class CreateByCartAmountShippingTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private CreateByCartAmountShipping $action;

    public function setUp(): void
    {
        parent::setUp();

        $this->action = $this->app->make(CreateByCartAmountShipping::class);
    }

    /**
     * @throws Throwable
     */
    public function testSePuedeCrearUnEnvioSegunImporteDelCarritoConEscalonesDePrecio(): void
    {
        $name = $this->faker->name();

        $this->action->execute(new ByCartAmountDto(
            $name,
            [(new StepFactory())->make(), (new StepFactory())->make()],
            (new ShippingZoneFactory())->createOne(),
        ));

        $shippingMethod = ByCartAmount::whereId(1)->firstOrFail();
        self::assertEquals($name, $shippingMethod->getName());
        self::assertNotEmpty($shippingMethod->getSteps());
    }
}
