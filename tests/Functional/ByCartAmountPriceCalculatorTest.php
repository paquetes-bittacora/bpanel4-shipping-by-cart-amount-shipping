<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Tests\Functional;

use Bittacora\Bpanel4\Orders\Database\Factories\CartFactory;
use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\PriceStep;
use Bittacora\Bpanel4\Shipping\Services\PriceCalculators\ByCartAmountPriceCalculator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Mockery\MockInterface;
use Tests\TestCase;

final class ByCartAmountPriceCalculatorTest extends TestCase
{
    use RefreshDatabase;

    private ByCartAmountPriceCalculator $priceCalculator;

    /**
     * @throws InvalidPriceException
     */
    public function testCalculaLosGastosDeEnvioSegunElImporteDelCarrito(): void
    {
        $priceCalculator = new ByCartAmountPriceCalculator(
            $this->getByCartAmountMock(),
            $this->getCartMock(20),
        );

        self::assertEquals(20, $priceCalculator->getShippingCosts()->toFloat());
    }

    /**
     * @throws InvalidPriceException
     */
    public function testNoEstaHabilitadoSiNoSeAlcanzaElMinimo(): void
    {
        $priceCalculator = new ByCartAmountPriceCalculator($this->getByCartAmountMock(), $this->getCartMock(1));

        self::assertFalse($priceCalculator->isEnabled());
    }

    /**
     * @throws InvalidPriceException
     */
    private function getCartMock(float $subtotal): Cart&MockInterface
    {
        $mock = Mockery::mock(Cart::class);
        $mock->shouldReceive('getSubtotalWithTaxes')->andReturn(new Price($subtotal));
        return $mock;
    }

    /**
     * @throws InvalidPriceException
     */
    private function getByCartAmountMock(): ByCartAmount&MockInterface
    {
        $mock = Mockery::mock(ByCartAmount::class);

        $steps = [];

        for ($i = 1; $i <= 3; ++$i) {
            $step = new PriceStep();
            $step->setMinimum(new Price(10 * $i));
            $step->setPrice(new Price(10 * $i));
            $steps[] = $step;
        }

        $mock->shouldReceive('getSteps')->andReturn(new Collection($steps));

        return $mock;
    }
}
