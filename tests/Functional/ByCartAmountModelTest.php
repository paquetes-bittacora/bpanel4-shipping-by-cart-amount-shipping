<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Tests\Functional;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Database\Factories\ShippingZoneFactory;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\PriceStep;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

final class ByCartAmountModelTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /**
     * @throws InvalidPriceException
     */
    public function testSePuedeAsociarElModeloALosEscalonesDePrecio(): void
    {
        $shippingMethod = new ByCartAmount();
        $shippingMethod->setShippingZone((new ShippingZoneFactory())->createOne());
        $shippingMethod->setName($this->faker->name());
        $shippingMethod->save();

        $step = new PriceStep();
        $step->setMinimum(new Price(100));
        $step->setPrice(new Price(10));
        $shippingMethod->addStep($step);

        $shippingMethod->refresh();
        $steps = $shippingMethod->getSteps();

        self::assertInstanceOf(Collection::class, $steps);
        self::assertCount(1, $steps);
        self::assertInstanceOf(PriceStep::class, $steps[0]);
    }
}
