<?php

declare(strict_types=1);

return [
    'name' => 'Nombre',
    'create-by-cart-amount' => 'Crear envío según importe del carrito',
    'edit-by-cart-amount' => 'Editar',
];
