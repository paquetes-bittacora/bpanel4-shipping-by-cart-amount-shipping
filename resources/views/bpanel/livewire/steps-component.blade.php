<div>
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label for="id-form-field-1" class="mb-0  ">
                <span class="text-danger">*</span>  Tramos de precio
            </label>
        </div>
        <div class="col-sm-7">
            <div class="input-group by-cart-amount-steps mb-2">
                @foreach($steps as $key => $step)
                    <div class="form-row row mb-2 w-100">
                        <div class="input-group col-5">
                            <label class="w-100 col-form-label">Importe mínimo</label>
                            <input type="number" name="steps[{{ $key }}][minimum]" step="0.01" min="0" wire:model="steps.{{$key}}.minimum" class="form-control">
                        </div>
                        <div class="input-group col-5">
                            <label class="w-100 col-form-label">Precio</label>
                            <input type="number" name="steps[{{ $key }}][price]" step="0.01" min="0" wire:model="steps.{{$key}}.price" class="form-control">
                        </div>
                        <div class="col-2 d-flex justify-content-center align-items-end">
                            <button wire:click.prevent="removeRow({{$key}})" class="remove">X</button>
                        </div>
                    </div>
                @endforeach
            </div>
            <button class="btn btn-primary" wire:click.prevent="addStep"><i class="fas fa-plus"></i> Añadir escalón</button>
        </div>
    </div>
    @push('styles')
        <link rel="stylesheet" href="{{ Vite::asset('vendor/bittacora/bpanel4-shipping-by-cart-amount/resources/assets/scss/by-cart-amount-shipping.scss') }}"/>
    @endpush
</div>
