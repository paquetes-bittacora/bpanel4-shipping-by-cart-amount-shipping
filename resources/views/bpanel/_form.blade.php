<form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
    @csrf
    @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-by-cart-amount::form.name'),
    'required'=>true, 'value' => old('name') ?? $model?->getName() ])
    @livewire('bpanel4-by-cart-amount::steps-component', ['initialSteps' => $model?->getSteps()])
    <div class="col-12 mt-3 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
    @if (null !== $model)
        <input type="hidden" name="id" value="{{ $model->getId() }}">
        <input type="hidden" name="shipping_zone_id" value="{{ $model->getShippingZoneId() }}">
    @else
        <input type="hidden" name="shipping_zone_id" value="{{ $zone->getId() }}">
    @endif
</form>
@if($errors->any())
    {{ implode('', $errors->all('<div>:message</div>')) }}
@endif
