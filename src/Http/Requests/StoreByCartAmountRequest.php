<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

final class StoreByCartAmountRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /** @return array<mixed> */
    public function rules(): array
    {
        return [
            'name' => 'string|required',
            'steps.*.minimum' => 'numeric|required|min:0',
            'steps.*.price' => 'numeric|required|min:0',
        ];
    }
}
