<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Http\Livewire;

use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\PriceStep;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

final class StepsComponent extends Component
{
    public array $steps = [
        ['minimum' => 0, 'price' => 0]
    ];

    /**
     * @param Collection<PriceStep>|null $initialSteps
     * @return void
     */
    public function mount(?Collection $initialSteps): void
    {
        if (null !== $initialSteps) {
            $this->steps = [];

            foreach ($initialSteps as $step) {
                $this->steps[] = [
                    'minimum' => $step->getMinimum()->toFloat(),
                    'price' => $step->getPrice()->toFloat(),
                ];
            }
        }
    }

    public function render(): View
    {
        usort($this->steps, static fn(array $a, array $b): int => $a['minimum'] <=> $b['minimum']);
        return view('bpanel4-by-cart-amount::bpanel.livewire.steps-component');
    }

    public function addStep(): void
    {
        $this->steps[] = [
            'minimum' => max(array_column($this->steps, 'minimum')),
            'price' => max(array_column($this->steps, 'price')),
        ];
    }

    public function removeRow(int $key)
    {
        unset($this->steps[$key]);
    }
}
