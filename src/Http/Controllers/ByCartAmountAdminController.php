<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Actions\CreateByCartAmountShipping;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Actions\UpdateByCartAmountShipping;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Dtos\ByCartAmountDto;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Http\Requests\StoreByCartAmountRequest;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Http\Requests\UpdateByCartAmountRequest;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory;
use Throwable;

final class ByCartAmountAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
        private readonly UrlGenerator $urlGenerator,
    ) {
    }

    /**
     * @throws AuthorizationException
     */
    public function create(ShippingZone $zone): View
    {
        $this->authorize('bpanel4-shipping.by-cart-amount.bpanel.create');

        return $this->view->make('bpanel4-by-cart-amount::bpanel.create', [
            'zone' => $zone,
            'model' => null,
            'action' => $this->urlGenerator->route(
                'bpanel4-shipping.by-cart-amount.bpanel.store',
                ['zone' => $zone]
            ),
        ]);
    }

    /**
     * @throws AuthorizationException
     */
    public function store(
        StoreByCartAmountRequest $request,
        ShippingZone $zone,
        CreateByCartAmountShipping $createByCartAmountShipping
    ): RedirectResponse {
        $this->authorize('bpanel4-shipping.by-cart-amount.bpanel.store');

        try {
            $dto = ByCartAmountDto::fromArray($request->validated() + ['zone' => $zone]);
            $byCartAmount = $createByCartAmountShipping->execute($dto);
            return $this->redirector->route('bpanel4-shipping.by-cart-amount.bpanel.edit', ['model' => $byCartAmount])
                ->with([
                    'alert-success' => 'La configuración del envío se ha guardado correctamente',
                ]);
        } catch (Throwable) {
            return $this->redirector->back()->with([
                'alert-danger' => 'Ocurrió un error al guardar la configuración',
            ]);
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(ByCartAmount $model): View
    {
        $this->authorize('bpanel4-shipping.by-cart-amount.bpanel.edit');

        return $this->view->make('bpanel4-by-cart-amount::bpanel.edit', [
            'zone' => $model->getShippingZoneId(),
            'model' => $model,
            'action' => $this->urlGenerator->route(
                'bpanel4-shipping.by-cart-amount.bpanel.update',
                ['model' => $model]
            ),
        ]);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(
        UpdateByCartAmountRequest $request,
        ByCartAmount $model,
        UpdateByCartAmountShipping $updateByCartAmount
    ): RedirectResponse {
        $this->authorize('bpanel4-shipping.by-cart-amount.bpanel.update');
        try {
            $dto = ByCartAmountDto::fromArray($request->validated() + ['zone' => $model->getShippingZone()]);
            $updateByCartAmount->execute($dto, $model);
            return $this->redirector->route('bpanel4-shipping.by-cart-amount.bpanel.edit', ['model' => $model->refresh()])
                ->with([
                    'alert-success' => 'La configuración del envío se ha guardado correctamente',
                ]);
        } catch (Throwable $exception) {
            return $this->redirector->route('bpanel4-shipping.by-cart-amount.bpanel.edit', ['model' => $model])
                ->with([
                'alert-danger' => 'Ocurrió un error al guardar la configuración',
            ]);
        }
    }

    public function destroy(ByCartAmount $model): RedirectResponse
    {
        try {
            $model->steps()->delete();
            $model->delete();
            return $this->redirector->back()->with(['alert-success' => 'Método de envío eliminado']);
        } catch (Throwable) {
            return $this->redirector->back()
                ->with(['alert-danger' => 'Ocurrió un error al intentar eliminar el método de envío']);
        }
    }
}
