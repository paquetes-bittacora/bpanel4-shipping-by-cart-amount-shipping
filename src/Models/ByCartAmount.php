<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Models\ShippingMethods;

use Bittacora\Bpanel4\Prices\Casts\PriceCast;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Bpanel4\Shipping\Traits\HasShippingClasses;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount
 *
 * @property int $id
 * @property int $shipping_zone_id
 * @property string $name
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ByCartAmount newModelQuery()
 * @method static Builder|ByCartAmount newQuery()
 * @method static Builder|ByCartAmount query()
 * @method static Builder|ByCartAmount whereActive($value)
 * @method static Builder|ByCartAmount whereCreatedAt($value)
 * @method static Builder|ByCartAmount whereId($value)
 * @method static Builder|ByCartAmount whereName($value)
 * @method static Builder|ByCartAmount whereShippingZoneId($value)
 * @method static Builder|ByCartAmount whereUpdatedAt($value)
 * @mixin Eloquent
 */
final class ByCartAmount extends Model implements ShippingMethod
{
    use HasShippingClasses;
    /** @var string $table */
    public $table = 'shipping_methods_by_cart_amount';

    /**
     * @var array<string, string|class-string<PriceCast>>
     */
    protected $casts = [
        'id' => 'int',
        'active' => 'bool',
    ];

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShippingZoneId(): int
    {
        return $this->shipping_zone_id;
    }

    public function setShippingZone(ShippingZone $shippingZone): void
    {
        $this->shipping_zone_id = $shippingZone->getId();
    }

    public function getShippingZone(): ShippingZone
    {
        return ShippingZone::whereId($this->shipping_zone_id)->firstOrFail();
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return HasMany<PriceStep>
     */
    public function steps(): HasMany
    {
        return $this->hasMany(PriceStep::class);
    }

    public function addStep(PriceStep $step): void
    {
        $this->steps()->save($step);
    }

    /**
     * @return Collection<PriceStep>
     */
    public function getSteps(): Collection
    {
        return $this->steps()->orderBy('minimum')->get();
    }

    /**
     * @param array<PriceStep> $steps
     */
    public function setSteps(array $steps): void
    {
        $this->steps()->delete();
        $this->steps()->saveMany($steps);
    }
}
