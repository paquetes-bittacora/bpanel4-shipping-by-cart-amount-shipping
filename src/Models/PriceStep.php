<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Models\ShippingMethods;

use Bittacora\Bpanel4\Prices\Casts\PriceCast;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Bittacora\Bpanel4\Shipping\Models\ShippingMethods\PriceStep
 *
 * @property int $id
 * @property int $by_cart_amount_id
 * @property int $minimum
 * @property int $price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|PriceStep newModelQuery()
 * @method static Builder|PriceStep newQuery()
 * @method static Builder|PriceStep query()
 * @method static Builder|PriceStep whereCreatedAt($value)
 * @method static Builder|PriceStep whereId($value)
 * @method static Builder|PriceStep whereIdShippingMethodByCartAmount($value)
 * @method static Builder|PriceStep whereMinimum($value)
 * @method static Builder|PriceStep wherePrice($value)
 * @method static Builder|PriceStep whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final class PriceStep extends Model
{
    public $table = 'shipping_methods_by_cart_amount_steps';

    /**
     * @var array<string, string>
     */
    protected $casts = [
        'id' => 'int',
        'minimum' => PriceCast::class,
        'price' => PriceCast::class,
    ];

    public function getMinimum(): Price
    {
        return $this->minimum;
    }

    public function setMinimum(Price $minimum): void
    {
        $this->minimum = $minimum;
    }

    public function getPrice(): Price
    {
        return $this->price;
    }

    public function setPrice(Price $price): void
    {
        $this->price = $price;
    }
}
