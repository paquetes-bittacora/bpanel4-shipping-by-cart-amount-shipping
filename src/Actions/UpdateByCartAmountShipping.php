<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Actions;

use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Dtos\ByCartAmountDto;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount;
use Illuminate\Database\Connection;
use Throwable;

final class UpdateByCartAmountShipping
{
    public function __construct(private  readonly Connection $db)
    {
    }

    /**
     * @throws Throwable
     */
    public function execute(ByCartAmountDto $dto, ByCartAmount $byCartAmount): ByCartAmount
    {
        $this->db->beginTransaction();
        try {
            $byCartAmount->setName($dto->name);
            $byCartAmount->setShippingZone($dto->zone);
            $byCartAmount->save();
            $byCartAmount->setSteps($dto->steps);
            $this->db->commit();

            return $byCartAmount->refresh();
        } catch (Throwable $e) {
            $this->db->rollBack();
            throw $e;
        }
    }
}
