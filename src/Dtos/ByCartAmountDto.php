<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Dtos;


use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\PriceStep;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Dtos\Dto;

final class ByCartAmountDto  extends Dto
{
    /**
     * @param string $name
     * @param array<PriceStep> $steps
     */
    public function __construct(
        public readonly string $name,
        public readonly array $steps,
        public readonly ShippingZone $zone,
    )
    {
    }

    protected static function getCustomFieldCasts(): array
    {
        return [
            'steps' => self::castToStepsArray(...),
        ];
    }

    /**
     * @throws InvalidPriceException
     */
    private static function castToStepsArray(string $field, string $type, array $value, bool $allowsNull): array
    {
        $output = [];

        foreach ($value as $step) {
            $priceStep = new PriceStep();
            $priceStep->setMinimum(new Price((float) $step['minimum']));
            $priceStep->setPrice(new Price((float) $step['price']));
            $output[] = $priceStep;
        }

        return $output;
    }
}
