<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services\PriceCalculators;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\PriceStep;

final class ByCartAmountPriceCalculator implements ShippingMethodPriceCalculator
{
    public function __construct(
        protected readonly ShippingMethod|ByCartAmount $shippingMethod,
        protected readonly Cart $cart
    )
    {
    }

    public function getOptionText(): string
    {
        return 'Según importe del carrito';
    }

    /**
     * @throws InvalidPriceException
     */
    public function isEnabled(): bool
    {
        $steps = $this->shippingMethod->getSteps();

        if (count($steps) === 0) {
            return false;
        }

        /** @var ?PriceStep $first */
        $first = $steps->first();
        if ($first?->getMinimum()->toFloat() > $this->cart->getSubtotalWithTaxes()->toFloat()) {
            return false;
        }

        return true;
    }

    /**
     * @throws InvalidPriceException
     */
    public function getShippingCosts(): ?Price
    {
        $subtotal = $this->cart->getSubtotalWithTaxes();
        $steps = $this->shippingMethod->getSteps();
        $price = null;

        foreach ($steps as $step) {
            if ($step->getMinimum()->toFloat() <= $subtotal->toFloat()) {
                $price = $step->getPrice();
            }
        }

        return $price;
    }
}
