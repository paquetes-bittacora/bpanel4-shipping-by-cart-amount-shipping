<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping;

use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Commands\InstallCommand;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Commands\RegisterTranslationsCommand;
use Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Http\Livewire\StepsComponent;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\ByCartAmount;
use Illuminate\Support\ServiceProvider;
use Bittacora\Bpanel4\Shipping\ShippingFacade;
use Livewire\LivewireManager;

final class ByCartAmountShippingServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-by-cart-amount';

    public function boot(LivewireManager $livewire): void
    {
        $this->commands( [InstallCommand::class, RegisterTranslationsCommand::class]);
        ShippingFacade::registerShippingMethod('Según importe del carrito', ByCartAmount::class);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);

        $livewire->component(self::PACKAGE_PREFIX . '::steps-component', StepsComponent::class);
    }
}
