<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Commands;

use Illuminate\Console\Command;
use RuntimeException;

final class RegisterTranslationsCommand extends Command
{
    /** @var string */
    protected $signature = 'bpanel4-by-cart-amount-shipping:register-translations';

    /** @var string */
    protected $description = 'Registra algunas traducciones globales que no pueden hacerse desde dentro del paquete';

    public function handle(): void
    {
        $dir = base_path() . '/resources/lang/vendor/bpanel4-shipping/es';
        $file = $dir . '/breadcrumbs.php';

        $this->createFolder($dir);
        $this->createFile($file);
        $this->writeTranslations($file);
    }

    private function writeTranslations(string $file): void
    {
        $contents = file_get_contents($file);
        $contents = preg_replace(
            '/(.+?)(\];)/s',
            "$1    'by-cart-amount' => 'Según importe del carrito',\n$2",
            $contents
        );
        file_put_contents($file, $contents);
    }

    private function createFolder(string $dir): void
    {
        if (!is_dir($dir) && !mkdir($dir, 0775, true) && !is_dir($dir)) {
            throw new RuntimeException(sprintf('No se pudo crear la carpeta %s', $dir));
        }
    }

    private function createFile(string $file): void
    {
        if (!is_file($file)) {
            touch($file);
            file_put_contents($file, "<?php\n\nreturn [\n];\n");
        }
    }
}
