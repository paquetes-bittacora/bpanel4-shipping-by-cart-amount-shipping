<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\ByCartAmountShipping\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

final class InstallCommand extends Command
{
    /** @var string */
    protected $signature = 'bpanel4-by-cart-amount-shipping:install';

    /** @var string */
    protected $description = 'Instala el método de envío según el importe del carrito';

    private const PERMISSIONS = ['index', 'create', 'edit', 'delete', 'store', 'update', 'destroy'];

    public function handle(): void
    {
        $this->giveAdminPermissions();
    }

    private function giveAdminPermissions(): void
    {
        $this->comment('Añadiendo permisos...');
        $adminRole = Role::findOrCreate('admin');
        foreach (self::PERMISSIONS as $permission) {
            $permission = Permission::firstOrCreate(['name' => 'bpanel4-shipping.by-cart-amount.bpanel.' . $permission]);
            $adminRole->givePermissionTo($permission);
        }
    }
}
